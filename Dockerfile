FROM python:latest
RUN pip install tgcrypto pyrogram
RUN mkdir /bot
WORKDIR /bot
COPY ["bot.py", "/bot/bot.py"]
COPY ["config.ini.example", "/bot/config.ini"]
CMD ["python3", "/bot/bot.py"]
